#ifndef GBA_GENERATED_BMP_TITLE_SCREEN_H
#define GBA_GENERATED_BMP_TITLE_SCREEN_H

#include <bitmap.h>

namespace persona_x::resources {

/*
const auto title_screen = gba::graphics::make_bitmap(
        std::uint16_t{3}, std::uint16_t{2},
        std::make_array<std::uint8_t>(
            std::uint8_t{1u},
            std::uint8_t{2u},
            std::uint8_t{3u},
            std::uint8_t{4u},
            std::uint8_t{5u},
            std::uint8_t{0u}
        ),
        std::make_array<std::uint16_t>(
            std::uint16_t{1u},
            std::uint16_t{2u},
            std::uint16_t{3u},
            std::uint16_t{4u},
            std::uint16_t{5u},
            std::uint16_t{6u},
            std::uint16_t{7u}
        )
);
*/
    /*
static const auto title_screen = gba::graphics::Bitmap<212ul * 160ul, 15ul>(
    uint16_t{212}, uint16_t{160},
    std::from_raw<std::uint8_t>({
    */

extern const gba::graphics::Bitmap title_screen;

}


#endif
