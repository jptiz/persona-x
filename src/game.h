#ifndef GAME_H
#define GAME_H

#include <gba.h>

void game_initialize();
void game_update();
void sound_initialize();
void sound_update();

#endif

