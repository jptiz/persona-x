#ifndef SCENE_H
#define SCENE_H

#include "game.h"

class Scene {
public:
  virtual void initialize() = 0;
  virtual void update() = 0;
  virtual void terminate() = 0;
};

#endif
