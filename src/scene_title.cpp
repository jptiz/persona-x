#include "scene_title.h"
#include "../res/title_screen.h"
#include "../res/o_misterio_do_allstar.h"
#include "../res/logo.h"

#include <geometry.h>
#include <mode4.h>

void SceneTitle::initialize() {
}

void SceneTitle::update() {
    using namespace gba::video;
    using namespace gba::video::mode4;
    using namespace gba::io;
    using gba::geometry::Rect;

    static bool drawn = false;
    if (frame_count() > 120u and not drawn) {
        lcd_control_reg() |= LCDControl::FORCE_BLANK;

        vsync();

        clear_screen();

        using persona_x::resources::title_screen;

        lcd_control_reg() = (lcd_control_reg() & ~0b111) | 4;

        const auto& palette = title_screen.palette();

        copy_to_palette(palette, 1);
        draw(
            Rect{7u, 0u, title_screen.width(), title_screen.height()},
            title_screen.data(),
            1
        );

        vsync();

        lcd_control_reg() &= ~LCDControl::FORCE_BLANK;
        drawn = true;
    }
}

void SceneTitle::terminate() {

}
