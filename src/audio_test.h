#ifndef GBA_AUDIO_TEST_H
#define GBA_AUDIO_TEST_H

#include "sound.h"

void wait(unsigned frames) {
    for (auto i = 0u; i < frames; ++i) {
        gba::video::vsync();
        if (i & 0b1111) {
            using namespace gba::sound;
            play_note(Channel::TONE, Note::E, -3, 0b111111);
            play_note(Channel::TONE, Note::F, -3, 0b111111);
            play_note(Channel::TONE, Note::E, -3, 0b111111);
        }
    }
}

void play_note_for(gba::sound::Note note, int octave, unsigned frames) {
    using namespace gba::io;
    using namespace gba::sound;

    play_note(Channel::TONE_SWEEP, note, octave, (64 - frames)>>2);
    wait(frames);
}

void play_sound() {
    using namespace gba::io;
    using namespace gba::sound;

    master_volume_on(true);
    volume(7, 7);
    master_control() |= MasterControl::CHANNEL1_RIGHT;
    master_control() |= MasterControl::CHANNEL2_RIGHT;
    master_control() |= MasterControl::CHANNEL3_RIGHT;
    master_control() |= MasterControl::CHANNEL4_RIGHT;
    master_control() |= MasterControl::CHANNEL1_LEFT;
    master_control() |= MasterControl::CHANNEL2_LEFT;
    master_control() |= MasterControl::CHANNEL3_LEFT;
    master_control() |= MasterControl::CHANNEL4_LEFT;

    sound_control() |= SoundControl::VOLUME_100;

    square1_set(15, WaveDuty::_25);
    square2_set(15, WaveDuty::_75);

    // intro
    for (auto i = 0u; i < 2u; ++i) {
        play_note_for(Note::B, -2, 10u);
        play_note_for(Note::B, -2, 10u);
        wait(5u);
        play_note_for(Note::C, -1, 30u);
        wait(5u);

        play_note_for(Note::B, -2, 10u);
        play_note_for(Note::B, -2, 10u);
        wait(5u);
        for (auto j = 0u; j < 5u; ++j) {
            play_note_for(Note::D, -1, 4u);
        }
        wait(60u);
    }
}

#endif
