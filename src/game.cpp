#include "game.h"
#include "../res/logo.h"

#include <mode3.h>

void game_initialize() {
    using namespace gba::video;
    using namespace gba::video::mode3;
    using namespace gba::io;

    draw({0u, 0u, 240u, 160u}, LOGO_palette, LOGO_data);

    volatile auto& disp_count = *(std::uint16_t*)0x4000006;

    while (disp_count < 160u) {}

    lcd_control_reg() = 3 + LCDControl::BG2;
    sound_initialize();
}

void game_update() {
}

void sound_initialize() {

}

void sound_update() {

}
