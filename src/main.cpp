#include "scene_title.h"

using namespace gba::video;
using namespace gba::io;

int main() {
    lcd_control_reg() |= LCDControl::FORCE_BLANK;
    game_initialize();

    auto title = SceneTitle{};
    Scene& scene = title;

    scene.initialize();
    while (true) {
        vsync();
        game_update();
        scene.update();
        sound_update();
    }
}

extern "C" void __cxa_pure_virtual() {
    while (true) {}
}
