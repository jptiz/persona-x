#ifndef SCENE_TITLE_H
#define SCENE_TITLE_H

#include "scene.h"

class SceneTitle : public Scene {
public:
  void initialize() override;
  void update() override;
  void terminate() override;
};

#endif
