// <array> -*- C++ -*-
// vim:ft=cpp
#ifndef GBA_STDLIB_ARRAY_H
#define GBA_STDLIB_ARRAY_H

#pragma GCC system_header

#include "utility"
#include "cstddef"

namespace std {

namespace detail {

template <typename T, std::size_t N>
struct array_inner {
    using type = T[N];

    constexpr static T& ref(const type& array, std::size_t n) {
        return const_cast<T&>(array[n]);
    }
};

template <typename T>
struct array_inner<T, 0> {
    struct type {};

    constexpr static T& ref(const type& array, std::size_t n) {
        return *static_cast<T*>(nullptr);
    }
};

}

template <typename T, std::size_t N>
struct array {
    using value_type = T;
    using reference = value_type&;
    using const_reference = const value_type&;
    using pointer = value_type*;
    using const_pointer = value_type*;
    using iterator = pointer;
    using const_iterator = const_pointer;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using array_inner = detail::array_inner<T, N>;

    typename array_inner::type data_;

    constexpr std::size_t size() const {
        return N;
    }

    constexpr std::size_t max_size() const {
        return N;
    }

    constexpr bool empty() {
        return N == 0;
    }

    constexpr const_reference operator[](size_type i) const {
        return array_inner::ref(data_, i);
    }

    reference operator[](size_type i) {
        return array_inner::ref(data_, i);
    }

    constexpr const_reference front() const {
        return array_inner::ref(data_, 0);
    }

    reference front() {
        return array_inner::ref(data_, 0);
    }

    constexpr const_reference back() const {
        return array_inner::ref(data_, N-1);
    }

    reference back() {
        return array_inner::ref(data_, N-1);
    }

    const_pointer data() const {
        return &array_inner::ref(data_, 0);
    }

    pointer data() {
        return &array_inner::ref(data_, 0);
    }

    const_iterator begin() const {
        return data();
    }

    iterator begin() {
        return data();
    }

    const_iterator end() const {
        return const_iterator(data() + N);
    }

    iterator end() {
        return iterator(data() + N);
    }

    void fill(const value_type& val) {
        for (auto& e: data_) {
            e = val;
        }
    }

    void swap(array &other) {
        using std::swap;
        for (auto i = size_type{}; i < N; ++i) {
            swap(data_[i], other[i]);
        }
    }
};

template <typename T, std::size_t N>
void swap(array<T, N>& a, array<T, N>& b) {
    a.swap(b);
}

}

#endif
