#include "cstring"

extern "C" {

void std::memset(void* dst, int value, std::size_t size) {
    auto dst_ = reinterpret_cast<std::uint8_t*>(dst);
    for (auto i = 0u; i < size; ++i) {
        dst_[i] = static_cast<std::uint8_t>(value);
    }
}

void std::memcpy(void* destiny, const void* src, std::size_t size) {
    auto destiny_ = reinterpret_cast<std::uint8_t*>(destiny);
    auto src_ = reinterpret_cast<const std::uint8_t*>(src);
    for (auto i = 0u; i < size; ++i) {
        destiny_[i] = src_[i];
    }
}

}

