#ifndef GBA_VIDEO_IO_H
#define GBA_VIDEO_IO_H

#include "io.h"

namespace gba::io {

enum LCDControl {
    // 0-1-2: Mode 0~7;
    // 3: (Read-Only) 1 = GBC cart, 0 = GBA cart.
    PAGE_SELECT = 1<<4,
    OAM_HBLANK = 1<<5,
    OBJ_MAPPING2D = 1<<6,
    FORCE_BLANK = 1<<7,
    BG0 = 1<<8,
    BG1 = 1<<9,
    BG2 = 1<<10,
    BG3 = 1<<11,
    OBJ = 1<<12,
    WIN0 = 1<<13,
    WIN1 = 1<<14,
    WINOBJ = 1<<15,
};

// Control registers
inline auto &lcd_control_reg()
{
    return *register_at(0x04000000);
}

inline volatile auto &scan_count() {
    return *register_at(0x04000006);
}

}

#endif
