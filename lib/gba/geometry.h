#ifndef LIBGBA_GEOMETRY_H
#define LIBGBA_GEOMETRY_H

namespace gba::geometry {

struct Point {
    int x{0};
    int y{0};

    auto operator-() const {
        return Point{-x, -y};
    }

    auto operator+(const Point& offset) const {
        return Point{x + offset.x, y + offset.y};
    }

    auto& operator+=(const Point& offset) {
        x += offset.x;
        y += offset.y;
        return *this;
    }
};

struct Rect {
    int x;
    int y;
    int width;
    int height;

    auto right() const {
        return x + width;
    }

    auto bottom() const {
        return y + height;
    }

    auto operator+(const Point& offset) const {
        return Rect{x + offset.x, y + offset.y, width, height};
    }

    auto& operator+=(const Point& offset) {
        x += offset.x;
        y += offset.y;
        return *this;
    }
};

}

#endif
