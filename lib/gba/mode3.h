#ifndef LIBGBA_MODE3_H
#define LIBGBA_MODE3_H

#include <cstdint>

#include "geometry.h"

namespace gba::video::mode3 {


inline auto &vram(std::uint8_t x, std::uint8_t y) {
    return reinterpret_cast<std::uint16_t*>(0x06000000)[x + 240*y];
}

void draw(const geometry::Rect& rect, const std::uint16_t palette[], const std::uint8_t data[]);

}

#endif
