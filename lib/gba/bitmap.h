#ifndef LIBGBA_BITMAP_H
#define LIBGBA_BITMAP_H

#include <array>
#include <cstdint>
#include <memory>

namespace gba::graphics {

/**
 * Bitmpap color palette.
 */
class Palette {
public:
    /**
     * @param palette Color array.
     * @param size Color array size.
     */
    Palette(const std::uint16_t palette[],
            std::size_t size
    ):
        palette_{palette},
        size_{size}
    {}

    const auto& colors() const {
        return palette_;
    }

    /**
     * Accesses a single entry at given index.
     */
    auto operator[](std::size_t i) {
        return palette_[i];
    }

    /**
     * Reads a single entry at given index.
     */
    const auto operator[](std::size_t i) const {
        return palette_[i];
    }

    /**
     * Number of colors on palette.
     */
    auto size() const {
        return size_;
    }

private:
    std::size_t size_;
    const std::uint16_t* palette_;
};

/**
 * A bitmap image with palette.
 */
class Bitmap {
public:
    Bitmap(int width,
           int height,
           const std::uint8_t data[],
           const Palette& palette
    ):
        width_{width},
        height_{height},
        data_{data},
        palette_{palette}
    {}

    auto width() const {
        return width_;
    }

    auto height() const {
        return height_;
    }

    auto data() const {
        return data_;
    }

    auto palette() const {
        return palette_;
    }

    const auto operator[](std::size_t index) const {
        return data_[index];
    }

private:
    const int width_;
    const int height_;
    const std::uint8_t* data_;
    const Palette& palette_;
};

}

#endif
