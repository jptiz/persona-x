#ifndef LIBGBA_SOUND_H
#define LIBGBA_SOUND_H

#include <array>

#include "sound_io.h"

namespace gba::sound {

inline void volume(std::uint8_t left, std::uint8_t right) {
    gba::io::master_control() |= ((left & 7) << 4) | (right & 7);
}

enum class Channel {
    TONE_SWEEP = 0u,
    TONE,
    WAVE_OUTPUT,
    NOISE,
    DMA_A,
    DMA_B,
};

enum class Note {
    C = 0,
    CIS,
    D,
    DIS,
    E,
    F,
    FIS,
    G,
    GIS,
    A,
    BES,
    B,
};

inline auto rate(Note note, unsigned octave) {
    const auto rates = std::array<int, 12>{
        8013, 7566, 7144, 6742, // C , C#, D , D#
        6362, 6005, 5666, 5346, // E , F , F#, G
        5048, 4766, 4499, 4246  // G#, A , A#, B
    };
    return 2048 - (rates[(unsigned)note] >> (4 + octave));
}

// For 0x4000062
enum class WaveDuty {
    _12_5 = 0u,
    _25,
    _50,
    _75
};

inline void square1_set(std::uint8_t volume, WaveDuty duty) {
    gba::io::square1_wave() = ((volume & 0b1111) << 0xc) | ((unsigned)duty << 6);
}

inline void square2_set(std::uint8_t volume, WaveDuty duty) {
    gba::io::square2_wave() = ((volume & 0b1111) << 0xc) | ((unsigned)duty << 6);
}

// For 0x4000064 and 0x400006c
enum SoundXControl {
    TIMED = 1 << 14,
    RESET = 1 << 15,
};

// For 0x4000080
enum MasterControl {
    CHANNEL1_RIGHT = 1 << 8,
    CHANNEL2_RIGHT = 1 << 9,
    CHANNEL3_RIGHT = 1 << 10,
    CHANNEL4_RIGHT = 1 << 11,
    CHANNEL1_LEFT = 1 << 12,
    CHANNEL2_LEFT = 1 << 13,
    CHANNEL3_LEFT = 1 << 14,
    CHANNEL4_LEFT = 1 << 15,
};

// For 0x4000082
enum SoundControl {
  VOLUME_25 = 0,
  VOLUME_50 = 1<<0,
  VOLUME_100 = 1<<1,
  RATIO_A_100 = 1<<2,
  RATIO_B_100 = 1<<3,
  ENABLE_DSA_LEFT = 1<<8,
  ENABLE_DSA_RIGHT = 1<<9,
  SET_TIMER_A = 1<<10,
  RESET_A = 1<<11,
  ENABLE_DSB_LEFT = 1<<12,
  ENABLE_DSB_RIGHT = 1<<13,
  SET_TIMER_B = 1<<14,
  RESET_B = 1<<15,
};

// For 0x4000084
enum DMGControl {
  ACTIVE_SQUARE1 = 1<<0,
  ACTIVE_SQUARE2 = 1<<1,
  ACTIVE_WAVE = 1<<2,
  ACTIVE_NOISE = 1<<3,
  ENABLE_MASTER_SOUND = 1<<7, // Must be on for any sound to play
};

inline void loop_note(Channel channel, Note note, unsigned octave) {
    using namespace gba::io;

    auto play_note_ = [&] (auto& wave, auto& control) {
        control = 0;
        control |=
            SoundXControl::RESET |
            rate(note, octave);
    };

    switch (channel) {
        case Channel::TONE_SWEEP:
            play_note_(square1_wave(), square1_control());
            break;
        case Channel::TONE:
            play_note_(square2_wave(), square2_control());
            break;
    }
}

inline void play_note(Channel channel, Note note, unsigned octave, unsigned length) {
    using namespace gba::io;

    auto play_note_ = [&] (auto& wave, auto& control) {
        wave |= length & 0b111111;
        control = 0;
        control |=
            SoundXControl::TIMED |
            SoundXControl::RESET |
            rate(note, octave);
    };

    switch (channel) {
        case Channel::TONE_SWEEP:
            play_note_(square1_wave(), square1_control());
            break;
        case Channel::TONE:
            play_note_(square2_wave(), square2_control());
            break;
    }
}

}

#endif
