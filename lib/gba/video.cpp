#include "video.h"

#include <cstring>

using namespace gba::io;

namespace {

auto frame_count_ = 0u;

}

gba::video::frame_count_t gba::video::frame_count() {
    return frame_count_;
}

void gba::video::vsync() {
    while (scan_count() >= 160u);
    while (scan_count() < 160u);
    frame_count_++;
}

void gba::video::clear_screen() {
    std::memset((unsigned short*)0x06000000, 0, 240u*160u*2u);
}

