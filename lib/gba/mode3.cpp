#include "mode3.h"

using namespace gba::video;
using namespace gba::geometry;

void mode3::draw(const Rect& rect, const std::uint16_t palette[], const std::uint8_t data[]) {
    for (auto j = 0u; j < rect.height; ++j) {
        for (auto i = 0u; i < rect.width; ++i) {
            auto color = palette[data[i + rect.width*j]];
            if (color > 0u) {
                vram(i + rect.x, j+rect.y) = color;
            }
        }
    }
}

