#ifndef LIBGBA_GRAPHICS_H
#define LIBGBA_GRAPHICS_H

#include <cstdint>

#include "video_io.h"
#include "geometry.h"

namespace gba::video {

using frame_count_t = std::size_t;

frame_count_t frame_count();
void vsync();
void clear_screen();

}


#endif
