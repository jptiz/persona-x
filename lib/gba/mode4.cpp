#include "mode4.h"


using namespace gba::video;
using namespace gba::geometry;
using gba::graphics::Bitmap;
using gba::graphics::Palette;

void mode4::copy_to_palette(const Palette& palette, unsigned base_index) {
    const auto count = palette.size();
    for (auto i = 0; i < count; ++i) {
        palette_ram(i + base_index) = palette[i];
    }
}

void mode4::draw(const Rect& rect, const std::uint8_t data[], unsigned offset) {
    auto [x, y, w, h] = rect;
    for (auto j = 0u; j < rect.height; ++j) {
        for (auto i = 0u; i < rect.width >> 1; ++i) {
            auto pixel_index = i << 1;
            auto left = data[pixel_index + w*j] + offset;
            auto right = data[pixel_index + 1 + w*j] + offset;
            vram(i + x, j + y) = (right << 8) | left;
        }
    }
}
