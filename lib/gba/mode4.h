#ifndef LIBGBA_MODE4_H
#define LIBGBA_MODE4_H

#include <cstdint>

#include "geometry.h"
#include "bitmap.h"

namespace gba::video::mode4 {

inline auto& vram(unsigned x, unsigned y) {
    return reinterpret_cast<std::uint16_t*>(0x0600'0000)[x + 120*y];
}

inline auto& palette_ram(unsigned index) {
    return reinterpret_cast<std::uint16_t*>(0x0500'0000)[index];
}

void copy_to_palette(const gba::graphics::Palette& palette, unsigned base_index);
void draw(const geometry::Rect& rect, const std::uint8_t data[], unsigned offset);

}

#endif

