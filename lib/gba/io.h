#ifndef GBA_IO_H
#define GBA_IO_H

#include <cstdint>

namespace gba::io {

inline volatile auto register_at(std::uint32_t address) {
    return reinterpret_cast<std::uint16_t*>(address);
}

}

#endif
