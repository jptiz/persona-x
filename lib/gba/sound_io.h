#ifndef LIBGBA_SOUND_IO_H
#define LIBGBA_SOUND_IO_H

#include "io.h"

namespace gba::io {

inline auto& square1_wave() {
    return *register_at(0x4000062);
}

inline auto& square1_control() {
    return *register_at(0x4000064);
}

inline auto& square2_wave() {
    return *register_at(0x4000068);
}

inline auto& square2_control() {
    return *register_at(0x400006c);
}

inline auto& master_control() {
    return *register_at(0x4000080);
}

inline auto &sound_control()
{
    return *register_at(0x04000082);
}

inline void master_volume_on(bool set) {
    *register_at(0x4000084) = (set ? 1 : 0) << 7;
}

inline auto &dmg_control_reg()
{
  return *register_at(0x04000084);
}

}

#endif
