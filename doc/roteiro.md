Persona X - Roteiro
===================

Dados
-----

 * Plataforma: **GBA** ou SFML?
 * Game Time: ?? (quero pelo menos +5h)
 * Final LV: 45+
 * Stats Iniciais: 2~6
 * Stats Finais: 70~120
 * Playable Characters: João Paulo, Shadow, Flavio

Sistemas
--------

 * Personas
 * Mapas isométricos
 * Batalhas isométricas
 * Troca de personas in-battle
 * Velvet Room para guardar / pegar personas reservas
 * Algumas personas servem para todos, outra só para listas restritas de
   Players

Trama
-----

 - Manter AllStar;

 - Fazer Shadows:
    - Shadow Shadow (deve meter maior pauleira);
    - Shadow TIZ (it's me, mario);
    - Shadow Maximus? (se for, cheio dos mecanismos marotos - só não sei
      quais);
        - Status negativos especiais da batalha.

 - Algo melhor que a ideia dos tickets por favor;
 - Wesley deve ficar puto com shadow;
 - Battle against wesley? Seria bom? (é almighty (non-elemental), i.e. é foda);
    - Roubo de vida;

 - Pelo menos 5 players;

 - Fazer jogável: Eu, Shadow, Flavio;

Capítulo 1 - O mundo do AllStar
-------------------------------

### Dados

 * Inicial LV: 1
 * Final LV: 5
 * Atmosfera: Mistério
 * MidBoss: Mighty Shade
 * Boss: Merman

 - João Pedro precisa aparecer de alguma maneira!!!
 - Substituto para Tickets:  Suddenly Allstar aparece montado no quarto das
   pessoas.
 - Substituto para Shadow entregar Ticket:  ???

**Nota: Wesley precisa continuar puto com o Shadow!**

Ideias para o item acima:
 - No idea :v
 -

### Sketch

1. Povo no intervalo de aula (lugar = player dependant)
2. Alguém comenta a respeito do AllStar - E mencionam que as escolas que estão
   disponibilizando
3. Todos ficam curiosos
4. ???
5. Sequência de eventos meio genérica/padrão até o fim do dia
6. ???
7. Player acorda 0:00
8. Eletrônicos parados
9. Jogador pode controlar de boas (só tem que se direcionar pra sala com a TV)
10. Lá fora: Névoa (se der pra ver pela janela) ou muito frio (inserir
    arrepios)
11. AllStar do nada na sala (maybe deixar como trama que o AllStar só aparece
    ali nesse horário?)
12. "Ligue o AllStar" ressoa na cabeça do Player;
13. Player se nega a ligar;
14. Tontura começa;
15. Player se sente fraco e sonolento;
16. ~Dorme~
17. Acorda no AllStar (de preferência variando lugar conforme player) -
    Surrounded by shadows
18. Batalha
19. ~InBattle~ Persona desperta
20. Gotta find a way out
21. Um pouquinho de dungeon
22. **Mid-Boss: Guardinha com Escudo redondo maneiro e espadinha bonitinha**
23. **~InBattle~ Recruta 2º Player**
24. Mais um pouco de Dungeon
25. **Boss: Merman fortinho (fogo pela boca, magias de gelo)**
26. **~InBattle~ Recruta 3º Player**
27. Conversas a respeito de WTF is going on
28. Encontram pilar de luz na próxima sala
29. Conversas sobre ser a única coisa na sala
30. Igor (do persona) aparece e fala algumas coisinhas *levemente* importantes
    (não encher muito a cabeça do player)
31. Players perguntam o que raios está acontecendo
32. Igor ri, fala algo e some com um flash
33. Players decidem entrar no pilar
34. Estão de volta cada um na sua casa, decidem dormir
35. Fim do capítulo

Capítulo 2 - Em busca do Wesley
-------------------------------

