# Config
OUTPUT   := persona_x

CPPSTD   := c++17
OPTIMLV  := -O2
INCLUDE  :=
CXXFLAGS := -g

# Libraries
STDLIB   := lib/std
LIBGBA   := lib/gba

# Architecture
ARCH     := -mthumb -mthumb-interwork -fomit-frame-pointer -ffast-math -fno-exceptions -fno-rtti

# Toolchain
BIN      := $(DEVKITARM)/bin
SPECS    := $(DEVKITARM)/arm-none-eabi/lib/gba.specs

# Makefile default entry
.PHONY: default
default: $(OUTPUT)


# Library Rules
$(STDLIB)/stdlib.a:
	@echo Compiling stdlib...
	@make -C $(STDLIB)
	@cd -
	@echo
	@echo

$(LIBGBA)/libgba.a:
	@echo Compiling LIBGBA...
	make -C $(LIBGBA)
	cd -
	@echo
	@echo

libraries: stdlib libgba

stdlib: $(STDLIB)/stdlib.a
libgba: $(LIBGBA)/libgba.a

# Fixes

subdirs   := $(wildcard */)
sources   := $(wildcard *.cpp) $(wildcard $(addsuffix *.cpp,$(subdirs)))
objects   := $(patsubst %.cpp,%.o,$(sources))

DEFAULTFLAGS := $(ARCH) $(OPTIMLV) # -isystem $(STDLIB) -nostdlib
INCLUDE      += $(STDLIB) $(LIBGBA)
INCLUDEFLAGS := $(foreach dir,$(INCLUDE),-I$(wildcard $(dir)))

# Source Rules

build/%.o: src/%.s
	@echo Compiling $^:
	@mkdir -p build/
	$(BIN)/arm-none-eabi-gcc -o $@ -c $^ $(DEFAULTFLAGS) -specs=$(SPECS) $(INCLUDEFLAGS)
	@echo

build/%.o: res/%.cpp
	@echo Compiling $^:
	@mkdir -p build/
	$(BIN)/arm-none-eabi-g++ -o $@ -c $^ $(DEFAULTFLAGS) -std=$(CPPSTD) -specs=$(SPECS) $(INCLUDEFLAGS)
	@echo

$(objects) : %.o : %.cpp
	@echo Compiling $^:
	@mkdir -p build/
	$(BIN)/arm-none-eabi-g++ -o $@ -c $^ $(DEFAULTFLAGS) -std=$(CPPSTD) -specs=$(SPECS) $(INCLUDEFLAGS)
	@echo

$(OUTPUT): $(objects) $(LIBGBA)/libgba.a #$(STDLIB)/stdlib.a
	@echo Compiling to $@.elf:
	$(BIN)/arm-none-eabi-g++ -o $@.elf $^ -Ibuild -specs=$(SPECS) $(OPTIMLV)
	@echo
	@echo Compiling to $@.gba:
	$(BIN)/arm-none-eabi-objcopy -O binary $@.elf $@.gba
	@echo
	@echo Fixing ROM...
	$(BIN)/gbafix $@.gba


# Other
clean:
	rm -rf build $(OUTPUT).gba $(OUTPUT).elf $(objects)

clean_libs:
	make clean -C $(STDLIB)
	@echo
	make clean -C $(LIBGBA)
	@echo
